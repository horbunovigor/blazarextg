(function ($) {
  "use strict";
  // variables
  var layout = $(".layout"),
    header = $(".layout__header");
  // preloader
  preloader();
  function preloader() {
    layout.on("click", ".nav__link", function (event) {
      layout.removeClass("layout_ready-load");
      event.preventDefault();
      var linkLocation = this.href;
      setTimeout(function () {
        window.location = linkLocation;
      }, 500);
    });
    setTimeout(function () {
      layout.addClass("layout_ready-load");
    }, 0);
  }

  // Single slider init
  if ($(".slider").length) {
    // $(this).find(".slider__item").clone().appendTo($(this));
    $(".slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      swipeToSlide: true,
      infinite: true,
      arrows: true,
      speed: 1000,
      fade: true,
      dots: true,
      prevArrow:
        '<div class="slick-prev"><svg  class="arrow"  viewBox="0 0 16 16"  xmlns="http://www.w3.org/2000/svg"><path d="M16 7H3.83L9.42 1.41L8 0L0 8L8 16L9.41 14.59L3.83 9H16V7Z" /></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg class="arrow"  viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 0L6.59 1.41L12.17 7H0V9H12.17L6.59 14.59L8 16L16 8L8 0Z" /></svg></div>',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            arrows: false,
            dots: true,
          },
        },
      ],
    });
  }
  // Menu
  navInit();
  function navInit() {
    header.find(".header__burger").on("click", function () {
      $(this).closest(header).toggleClass("layout__header_menu-active");
    });
  }

  // Submenu
  dropDownInit();
  function dropDownInit() {
    header.find(".nav__ico").on("click", function () {
      $(this).closest(".nav__item_dropdown").toggleClass("nav__item_active");
    });
  }

  /// Scroll functions
  $(window).on("load resize scroll", function () {
    let h = $(window).height();
    scrollHeader(h);
    scrollSection(h);
    scrollImage(h);
  });
  function scrollHeader(h) {
    if ($(window).scrollTop() >= 1) {
      header.addClass("layout__header_scroll");
    } else {
      header.removeClass("layout__header_scroll");
    }
  }
  function scrollSection(h) {
    let section = $(".section");
    section.each(function () {
      if ($(window).scrollTop() + h >= $(this).offset().top) {
        $(this).addClass("section_animation");
      }
    });
  }
  function scrollImage(h) {
    // Image initialization
    let img = $("img");
    img.each(function () {
      if (
        $(window).scrollTop() + h >= $(this).offset().top &&
        this.getAttribute("data-src") &&
        this.src !== this.getAttribute("data-src")
      ) {
        this.src = this.getAttribute("data-src");
      }
    });
  }

  // Tabs init
  if ($(".tabs").length) {
    tabsInit();
  }

  function tabsInit() {
    let position,
      tabsActive = "tabs__item_active";
    $(".layout__tabs")
      .find(".tabs__header")
      .on("click", ".tabs__item", function () {
        position = $(this).index();
        $(this).addClass(tabsActive).siblings().removeClass(tabsActive);
        $(this)
          .closest(".layout__tabs")
          .find(".tabs__main")
          .find(".tabs__item")
          .eq(position)
          .slideDown(400)
          .siblings()
          .slideUp(400);
      });
  }

  // Accordion init
  if ($(".layout__accordion").length) {
    accordionInit();
  }

  function accordionInit() {
    $(".layout__accordion").on("click", ".accordion__header", function () {
      $(this)
        .closest(".accordion__item")
        .toggleClass("accordion__item_active")
        .siblings()
        .removeClass("accordion__item_active");
      $(this)
        .closest(".accordion__item")
        .find(".accordion__main")
        .slideToggle(200);
    });
  }

  // Validation & customize form
  if ($("form").length) {
    // validation init
    formValidation();
    // select init
    jcf.setOptions("Select", {
      wrapNative: false,
      wrapNativeOnMobile: false,
      fakeDropInBody: false,
      maxVisibleItems: 5,
    });
    // Number init
    jcf.setOptions("Number", {
      fakeStructure:
        '<span class="jcf-number"><span class="jcf-btn-dec"><svg width="10" height="2" viewBox="0 0 10 2" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M1 1L9 1" stroke="#006B54" stroke-width="2" stroke-linecap="round"/></svg></span><span class="jcf-btn-inc"><svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M5 1L5 9M9 5L1 5" stroke="#006B54" stroke-width="2" stroke-linecap="round"/></svg></span></span>',
    });
    jcf.replaceAll();
    // Mask form
    $(".form__item_mask").mask("8(999) 999-99-99");

    // Attach
    if ($(".attach").length) {
      attach();
    }
    function attach() {
      let attach = $(".attach"),
        attachList = attach.find(".attach__list"),
        attachItemEmpty = $(".attach").find(".attach__list").html();
      attach.find(".attach__list").html("");

      // init
      attach.on("click", ".attach__init", function () {
        attachList.append(attachItemEmpty);
        $(this)
          .closest(".attach")
          .find(attachList)
          .find(".attach__item")
          .last()
          .hide();
        $(this)
          .closest(".attach")
          .find(attachList)
          .find(".attach__item")
          .last()
          .find(".attach__input")
          .find(".input")
          .click();
      });
      // change
      attach.on("change", ".input", function () {
        $(this).closest(".attach__item").show();
        $(this)
          .closest(".attach__item")
          .find(".attach__title")
          .find(".title__text")
          .text($(this).prop("files")[0].name);
      });
      // remove
      attach.on("click", ".attach__action", function () {
        $(this).closest(".attach__item").remove();
      });
    }

    /* Slider */
    if ($(".range").length) {
      rangeInit();
    }
    function rangeInit() {
      $(".range__slider").slider({
        range: true,
        min: 1500,
        max: 10000,
        step: 100,
        values: [3000, 6000],
        slide: function (event, ui) {
          console.log(ui.values[0]);
          $(".range__input_min").val(ui.values[0]);
          $(".range__input_max").val(ui.values[1]);
        },
      });
      $(".range__input_min").val($(".range__slider").slider("values", 0));
      $(".range__input_max").val($(".range__slider").slider("values", 1));
    }

    /* Calendar */
    if ($(".datepicker").length) {
      datepickerInit();
    }
    function datepickerInit() {
      /* Датапикер */
      $.datepicker.regional["ru"] = {
        closeText: "Закрыть",
        currentText: "Сегодня",
        prevText:
          '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none"><path d="M13 4L7 10L13 16" stroke="#7A828A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>',
        nextText:
          '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none"><path d="M7 16L13 10L7 4" stroke="#7A828A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>',
        monthNames: [
          "Январь",
          "Февраль",
          "Март",
          "Апрель",
          "Май",
          "Июнь",
          "Июль",
          "Август",
          "Сентябрь",
          "Октябрь",
          "Ноябрь",
          "Декабрь",
        ],
        monthNamesShort: [
          "Янв",
          "Фев",
          "Мар",
          "Апр",
          "Май",
          "Июн",
          "Июл",
          "Авг",
          "Сен",
          "Окт",
          "Ноя",
          "Дек",
        ],
        dayNames: [
          "воскресенье",
          "понедельник",
          "вторник",
          "среда",
          "четверг",
          "пятница",
          "суббота",
        ],
        dayNamesShort: ["вск", "пнд", "втр", "срд", "чтв", "птн", "сбт"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        weekHeader: "Не",
        dateFormat: "dd.mm.yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: "",
      };
      $.datepicker.setDefaults($.datepicker.regional["ru"]);
      /* Датапикер Одиночный*/
      $(".datepicker").datepicker({
        showOtherMonths: true,
        minDate: 0,
      });

      /* Датапикер период*/
      $(".datepicker_range").datepicker({
        onSelect: function (selectedDate) {
          if (!$(this).data().datepicker.first) {
            $(this).data().datepicker.inline = true;
            $(this).data().datepicker.first = selectedDate;
          } else {
            if (selectedDate > $(this).data().datepicker.first) {
              $(this).val(
                $(this).data().datepicker.first + " - " + selectedDate
              );
            } else {
              $(this).val(
                selectedDate + " - " + $(this).data().datepicker.first
              );
            }
            $(this).data().datepicker.inline = false;
          }
        },
        onClose: function () {
          delete $(this).data().datepicker.first;
          $(this).data().datepicker.inline = false;
        },
      });
    }
  }

  function formValidation() {
    let form = $("form");
    form.submit(function () {
      if ($(this).valid()) {
        return true;
      } else {
        return false;
      }
    });
    form.validate({
      rules: {
        name: {
          required: true,
          name: true,
        },
        phone: {
          required: true,
        },
      },
    });
  }

  // MODAL INIT
  modalInit();
  function modalInit() {
    let modalName;
    // modal show
    $(document).on("click", ".modal-init", function () {
      layout
        .addClass("layout_modal-active")
        .find(".modal__layout")
        .removeClass("modal__layout_active");
      modalName = $(this).data("modalname");
      layout.find("." + modalName + "").addClass("modal__layout_active");
    });
    // modal hide
    $(document).mouseup(function (e) {
      if ($(".modal__layout_active").length) {
        var div = $(".modal__layout");
        if (!div.is(e.target) && div.has(e.target).length === 0) {
          modalHide();
        }
      }
    });
    // modal hide
    $(document).on("click", ".modal__action", function () {
      modalHide();
    });
    // modal hide
    $(window).keydown(function (e) {
      if (e.key === "Escape") {
        modalHide();
      }
    });

    function modalHide() {
      layout
        .removeClass("layout_modal-active")
        .find(".modal__layout")
        .removeClass("modal__layout_active");
    }
  }

  // Scroll
  linkScroll();
  function linkScroll() {
    $('a[href^="#"]:not([href="#"])').click(function (e) {
      e.preventDefault();
      var target = $($(this).attr("href"));
      if (target.length) {
        var scrollTo = target.offset().top;
        $("body, html").animate({ scrollTop: scrollTo + "px" }, 800);
      }
    });
  }
})(jQuery);
